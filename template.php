<?php

/**
 * @file
 * This file contains the main theme functions hooks and overrides.
 */

require_once dirname(__FILE__) . '/includes/utilities.inc';
require_once dirname(__FILE__) . '/includes/theme.inc';
require_once dirname(__FILE__) . '/includes/structure.inc';
require_once dirname(__FILE__) . '/includes/form.inc';
require_once dirname(__FILE__) . '/includes/menu.inc';
require_once dirname(__FILE__) . '/includes/comment.inc';
require_once dirname(__FILE__) . '/includes/view.inc';
require_once dirname(__FILE__) . '/includes/admin.inc';
require_once dirname(__FILE__) . '/includes/contrib.inc';

/**
 * Override or insert variables into the maintenance page template.
 */
function ama_dablam_preprocess_maintenance_page(&$vars) {
  // While markup for normal pages is split into page.tpl.php and html.tpl.php,
  // the markup for the maintenance page is all in the single
  // maintenance-page.tpl.php template. So, to have what's done in
  // ama_dablam_preprocess_html() also happen on the maintenance page, it has to be
  // called here.
  ama_dablam_preprocess_html($vars);
}

/**
 * Override or insert variables into the html template.
 */
function ama_dablam_preprocess_html(&$vars) {

  // Get admin folder path.
  $ama_dablam_path = drupal_get_path('theme', 'ama_dablam');

  // Add theme name to body class.
  $vars['classes_array'][] = 'admin-base-theme nav-md';

  $html5shiv = array (
    '#tag' => 'script',
    '#weight' => 10000,
    '#attributes' => array( // Set up an array of attributes inside the tag
      'src' => 'https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js', 
    ),
    '#prefix' => '<!--[if lte IE 9]>',
    '#suffix' => '</script><![endif]-->'
  );
  drupal_add_html_head($html5shiv, 'html5shiv');

  $respond = array (
    '#tag' => 'script',
    '#weight' => 10000,
    '#attributes' => array( // Set up an array of attributes inside the tag
      'src' => 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js', 
    ),
    '#prefix' => '<!--[if lte IE 9]>',
    '#suffix' => '</script><![endif]-->',
  );
  drupal_add_html_head($respond, 'respond');

  // Fix the viewport and zooming in mobile devices.
  $viewport = array(
   '#tag' => 'meta',
   '#attributes' => array(
     'name' => 'viewport',
     'content' => 'width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no, initial-scale=1',
   ),
  );
  drupal_add_html_head($viewport, 'viewport');

}

/**
 * Override or insert variables into the page template.
 */
function ama_dablam_preprocess_page(&$vars) {
  // Format and add administration menu to theme.
  $vars['manager_menu'] = '';
  $manager_menu = theme_get_setting('menu_machine_name');
  if ($manager_menu && variable_get('menu_main_links_source', $manager_menu)) {
    $vars['manager_menu'] = menu_tree(variable_get('menu_main_links_source', $manager_menu));
    $vars['manager_menu']['#theme_wrappers'] = array();
  }

  $vars['primary_local_tasks'] = $vars['tabs'];
  unset($vars['primary_local_tasks']['#secondary']);
  $vars['secondary_local_tasks'] = array(
    '#theme' => 'menu_local_tasks',
    '#secondary' => $vars['tabs']['#secondary'],
  );

}

/**
 * Display the list of available node types for node creation.
 */
function ama_dablam_node_add_list($variables) {
  $content = $variables['content'];
  $output = '';
  if ($content) {
    $output = '<ul class="admin-list">';
    foreach ($content as $item) {
      $output .= '<li class="clearfix">';
      $output .= '<span class="label">' . l($item['title'], $item['href'], $item['localized_options']) . '</span>';
      $output .= '<div class="description">' . filter_xss_admin($item['description']) . '</div>';
      $output .= '</li>';
    }
    $output .= '</ul>';
  }
  else {
    $output = '<p>' . t('You have not created any content types yet. Go to the <a href="@create-content">content type creation page</a> to add a new content type.', array('@create-content' => url('admin/structure/types/add'))) . '</p>';
  }
  return $output;
}

/**
 * Implements theme_tablesort_indicator().
 * 
 * Use our own image versions, so they show up as black and not gray on gray.
 */
function ama_dablam_tablesort_indicator($variables) {
  $style = $variables['style'];
  $theme_path = drupal_get_path('theme', 'ama_dablam');
  if ($style == 'asc') {
    return theme('image', array('path' => $theme_path . '/images/arrow-asc.png', 'alt' => t('sort ascending'), 'width' => 13, 'height' => 13, 'title' => t('sort ascending')));
  }
  else {
    return theme('image', array('path' => $theme_path . '/images/arrow-desc.png', 'alt' => t('sort descending'), 'width' => 13, 'height' => 13, 'title' => t('sort descending')));
  }
}

/**
 * Implements hook_css_alter().
 */
function ama_dablam_css_alter(&$css) {
  // Unset vertical tabs css.
  unset($css['misc/vertical-tabs.css']);
}

function ama_dablam_dashboard_region($variables) {
  extract($variables);
  $output = '<div id="' . $element['#dashboard_region'] . '" class="dashboard-region">';
  $output .= '<div class="region clearfix">';
  $output .= $element['#children'];
  // Closing div.region
  $output .= '</div>';
  // Closing div.dashboard-region
  $output .= '</div>';
  return $output;
}
