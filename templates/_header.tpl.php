<?php
$user = user_load($user->uid);
$avatar = (isset($user->picture->uri)) ? file_create_url($user->picture->uri) : file_create_url(drupal_get_path('theme', 'ama_dablam') . '/images/user.png');
?>
<div class="container body">
	<div class="main_container">
		<div class="col-md-3 left_col">
			<div class="left_col scroll-view">

				<div class="navbar nav_title" style="border: 0;">
					<a href="/" class="site_title">
						<i class="fa fa-area-chart"></i> 
						<span>
							<?php print $site_name; ?>
						</span>
					</a>
				</div>
				<div class="clearfix"></div>

				<!-- menu prile quick info -->
        <div class="profile clearfix">
        	<?php if($user->picture): ?>
	        	<div class="profile_pic">
	        		<img src="<?php print $avatar; ?>" alt="..." class="img-circle profile_img">
	        	</div>
	        <?php endif; ?>
        	<div class="profile_info">
        		<span><?php print t('Welcome'); ?>,</span>
        		<h2>
        			<?php print l($user->name, 'user/' . $user->uid . '/edit/'); ?>
        		</h2>
        	</div>
        </div>
        <!-- /menu prile quick info -->
        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        	<div class="menu_section">
        		<h3>
        			<?php print t('Manager menu'); ?>
        		</h3>
        		<?php if (isset($manager_menu)): ?>
					<ul id="main-menu" class="menu nav side-menu">
						<?php print render($manager_menu); ?>
					</ul>
				<?php endif; ?>

        		<?php 
        		if ($page) {
	        		if ($page['sidebar_first']) {
	        			print render($page['sidebar_first']);
	        		} 
	        	}
	        	?>
        	</div>
        </div>				
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
        	<a href="<?php print theme_get_setting('settings_url'); ?>" data-toggle="tooltip" data-placement="top" title="Settings">
        		<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
        	</a>
        	<a data-toggle="tooltip" data-placement="top" title="FullScreen">
        		<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
        	</a>
        	<a class="scrollToTop" data-toggle="tooltip" data-placement="top" title="scroll top">
        		<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
        	</a>
        	<a href="/user/logout" data-toggle="tooltip" data-placement="top" title="Logout">
        		<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
        	</a>
        </div>
        <!-- /menu footer buttons -->

			</div>
		</div>

		<!-- top navigation -->
		<div class="top_nav">
			<div class="nav_menu">
				<nav class="" role="navigation">
					<div class="nav toggle">
						<a id="menu_toggle"><i class="fa fa-bars"></i></a>
					</div>

					<ul class="nav navbar-nav navbar-right">
						<li class="">
							<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<?php if($user->picture): ?>
									<img src="<?php print $avatar; ?>" />
								<?php endif; ?>
								<?php print $user->name; ?>
								<span class=" fa fa-angle-down"></span>
							</a>
							<ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
								<li>
									<?php print l(t('Profile'), 'user/' . $user->uid . '/edit/'); ?>
								</li>
								<li>
									<?php print l('<span>' . t('Settings') . '</span>', 'admin/config/system/site-information', array('html' => true)); ?>
								</li>
								<li>
									<?php print l('<i class="fa fa-sign-out pull-right"></i>' . t('Log Out'), 'user/logout', array('html' => true)); ?>
								</li>
							</ul>
						</li>

						<li role="presentation" class="dropdown">
							<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
								<i class="fa fa-envelope-o"></i>
								<span class="badge bg-green">1</span>
							</a>

							<ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
								<li>
									<a>
										<span class="image">
											<img src="<?php print $avatar; ?>" />
										</span>
										<span>
											<span>John Smith</span>
											<span class="time">3 mins ago</span>
										</span>
										<span class="message">
											Film festivals used to be do-or-die moments for movie makers. They were where...
										</span>
									</a>
								</li>
							</ul>
						</li>
					</ul>
			</div>
		</div>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
		