/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function($) {
    /** ******  left menu  *********************** **/
    Drupal.behaviors.admin_left_menu = {
      attach: function (context, settings) {
        $('#sidebar-menu li').removeClass('active');

        $('#sidebar-menu .side-menu > li').once('side-menu-item', function(){
            $(this).click(function () {
                if ($(this).is('.active')) {
                    $(this).removeClass('active');
                    //$('ul', this).slideUp();
                    $(this).removeClass('nv');
                    $(this).addClass('vn');
                } else {
                    $('#sidebar-menu li ul').slideUp();
                    $(this).removeClass('vn');
                    $(this).addClass('nv');
                    $('ul', this).slideDown();
                    $('#sidebar-menu li').removeClass('active');
                    $(this).addClass('active');
                }
            });
        });

        $('#menu_toggle').once('menu-toggle', function(){
            $(this).on('click', function () {
                if ($('body').hasClass('nav-md')) {
                    $('body').removeClass('nav-md');
                    $('body').addClass('nav-sm');
                    $('.left_col').removeClass('scroll-view');
                    $('.left_col').removeAttr('style');
                    $('.sidebar-footer').hide();

                    if ($('#sidebar-menu li').hasClass('active')) {
                        $('#sidebar-menu li.active').addClass('active-sm');
                        $('#sidebar-menu li.active').removeClass('active');
                    }
                } else {
                    $('body').removeClass('nav-sm');
                    $('body').addClass('nav-md');
                    $('.sidebar-footer').show();

                    if ($('#sidebar-menu li').hasClass('active-sm')) {
                        $('#sidebar-menu li.active-sm').addClass('active');
                        $('#sidebar-menu li.active-sm').removeClass('active-sm');
                    }
                }
            });
        });
      }
    };

    /* Scroll top link */
    Drupal.behaviors.scroll_top = {
        attach: function(context, settings) {
            $('.scrollToTop').once('scroll-to-top', function(){
                $(this).click(function(){
                    $('html, body').animate({scrollTop : 0}, 500);
                    return false;
                });
            });
        }
    };

    /* Sidebar Menu active class */
    Drupal.behaviors.sidebar_menu_active_class = {
      attach: function (context, settings) {
        var url = window.location;
        $('#sidebar-menu a[href="' + url + '"]').parent('li').addClass('current-page');
        $('#sidebar-menu a').filter(function () {
            return this.href == url;
        }).parent('li').addClass('current-page').parent('ul').slideDown().parent().addClass('active');
      }
    };

    /** ******  /left menu  *********************** **/
    /** ******  tooltip  *********************** **/
    Drupal.behaviors.admin_tooltip = {
      attach: function (context, settings) {
        $('[data-toggle="tooltip"]').tooltip();
      }
    };

    /** ******  /tooltip  *********************** **/
    /** ******  progressbar  *********************** **/
    Drupal.behaviors.admin_progress_bar = {
      attach: function (context, settings) {    
        if ($(".progress .progress-bar")[0]) {
            $('.progress .progress-bar').progressbar(); // bootstrap 3
        }
      }
    };

    /** ******  /progressbar  *********************** **/
    /** ******  collapse panel  *********************** **/
    Drupal.behaviors.admin_collapse_panel = {
      attach: function (context, settings) {
        if ($(".x_panel")[0]) {
            $('.x_panel').each(function(){
                var button = $(this).find('.collapse-link i');
                if ($(this).hasClass('.collapsed')) {
                    button.toggleClass('fa-chevron-up')
                    .toggleClass('fa-chevron-down');
                }
            });
        }

        // Close ibox function
        $('.close-link').once('close-link', function(){
            $(this).on('click', function () {
                var content = $(this).closest('.x_panel');
                content.remove();
            });
        });

        // Collapse ibox function
        $('.x_title').once('x_title', function(){
            $(this).on('click', function () {
                var x_panel = $(this).closest('.x_panel');
                var button = x_panel.find('.collapse-link i');
                var content = x_panel.find('div.x_content');
                content.slideToggle(200);
                (x_panel.hasClass('fixed_height_390') ? x_panel.toggleClass('').toggleClass('fixed_height_390') : '');
                (x_panel.hasClass('fixed_height_320') ? x_panel.toggleClass('').toggleClass('fixed_height_320') : '');
                button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                setTimeout(function () {
                    x_panel.resize();
                }, 50);
            });
        });
      }
    };
    /** ******  /collapse panel  *********************** **/
    /** ******  Accordion  *********************** **/
    Drupal.behaviors.admin_accordion = {
      attach: function (context, settings) {
        $(".expand").once('expand', function(){
            $(this).on("click", function () {
                $(this).next().slideToggle(200);
                $expand = $(this).find(">:first-child");

                if ($expand.text() == "+") {
                    $expand.text("-");
                } else {
                    $expand.text("+");
                }
            });
        });
      }
    };
    /** ******  Accordion  *********************** **/

    /** ******  Upload button  *********************** **/
    Drupal.behaviors.UploadButton = {
      attach: function (context) {
        $(':file', context).each(function(){
          var $file = $(this, context),
            $parent = $(this).parent(),
            $submit = $('.form-submit', $parent);

          $file.once('upload', function() {
            $parent.prepend('<button class="btn btn-default upload">'+Drupal.t('Upload')+'</button>');
            $submit.hide();
            $file.hide();
          });

          $parent.find('.btn.upload').once('upload', function(){
            $(this).on('click', function(e){
              $file.click();
              e.preventDefault();
            });
          });
          

          $file.change(function(){
            $parent.find('.btn.upload').removeClass('upload-processed');
            $('.alert-danger').remove();

            $submit.mousedown();
          });
        });
      }
    } 
    /** ******  Upload button  *********************** **/
})(jQuery);