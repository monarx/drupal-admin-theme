<?php


/**
 * @file
 * Theme setting callbacks for the Adminimal theme.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function ama_dablam_form_system_theme_settings_alter(&$form, &$form_state) {

	$form['theme_settings'] = array(
		'#type' => 'fieldset',
		'#title' => t('Theme Settings'),
		'#collapsible' => FALSE,
		'#collapsed' => FALSE,
	);
		$form['theme_settings']['tabs'] = array(
			'#type' => 'vertical_tabs',
	  );
	  	$form['theme_settings']['tabs']['basic_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('Basic Settings'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
				$form['theme_settings']['tabs']['basic_settings']['display_icons_config'] = array(
					'#type' => 'checkbox',
					'#title' => t('Display icons in Configuration page'),
					'#default_value' => theme_get_setting('display_icons_config'),
				);
				$form['theme_settings']['tabs']['basic_settings']['menu_machine_name'] = array(
					'#type' => 'textfield',
					'#title' => t('Manager menu machine name'),
					'#default_value' => theme_get_setting('menu_machine_name'),
					'#description' => t('Display menu in Left sidebar on admin page.'),
				);
				$form['theme_settings']['tabs']['basic_settings']['settings_url'] = array(
					'#type' => 'textfield',
					'#title' => t('Settings url'),
					'#default_value' => theme_get_setting('settings_url'),
					'#description' => t('Enter settings link url'),
				);

}
